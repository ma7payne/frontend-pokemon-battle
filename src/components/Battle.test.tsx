import React from 'react';
import { render } from '@testing-library/react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Home from './Battle'

Enzyme.configure({ adapter: new Adapter() });

describe('Testing over Home component', () => {

  test('Should contain element text "Pokemon Battle!"', () => {
    const wrapper = render(<Home />)
    expect(wrapper.getByText('Pokemon Battle!')).toBeInTheDocument()
  })

  test('Should contain <h1> element', () => {
    const wrapper = shallow(<Home />)
    expect(wrapper.find('h1').exists()).toBe(true)
  })

  test('Should contain only one <h1> element', () => {
    const wrapper = shallow(<Home />)
    expect(wrapper.find('h1')).toHaveLength(1)
  })

  test('Should contain element <h1>Pokemon Battle!</h1>', () => {
    const wrapper = shallow(<Home />)
    expect(wrapper.find('h1').html()).toBe('<h1>Pokemon Battle!</h1>')
  })

  test('Should exist 4 options in select form input', () => {
    const wrapper = shallow(<Home />)
    expect(wrapper.find('select').first().childAt(3).html()).not.toBeNull()
  })
})