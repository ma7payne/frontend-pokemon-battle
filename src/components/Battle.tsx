import React, { FormEvent, useState } from 'react'
import './Battle.css'

import { getDamage } from '../services/Damage'

const Home = () => {

  const typeOptions = [
    { name: 'Fire 🔥', value: 'fire' },
    { name: 'Water 💧', value: 'water' },
    { name: 'Grass 🌱', value: 'grass' },
    { name: 'Electric ⚡', value: 'electric' }
  ]

  const [damage, setDamage] = useState(0)
  const [valid, setValid] = useState(false)
  const [submit, setSubmit] = useState(false)

  const validateForm = (e: FormEvent<HTMLFormElement>) => {
    const { attack, defense } = e.currentTarget

    if (!!(attack.value) && !!(defense.value)) {
      if (attack.value >= 0 && defense.value >= 0)
        return true
      else return false
    }
    else return false
  }

  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault()

    if (validateForm(e)) {
      setValid(true)
      setSubmit(true)

      const { type, attack, defense, opponentType } = e.currentTarget
      const args = {
        type: type.value,
        attack: attack.value,
        defense: defense.value,
        opponentType: opponentType.value
      }

      getDamage(args).then((damage) => {
        setDamage(damage)
      })
    } else {
      setValid(false)
      setSubmit(true)
    }
  }

  return (
    <div className="container text-center">
      <div className="row align-items-center">
        <div className="col">
          <div className="title">
            <h1>Pokemon Battle!</h1>
          </div>
          <form onSubmit={handleSubmit} noValidate>
            <div className="row justify-content-center">
              <div className="col col-lg-5">
                <div>
                  <h2 className="text-uppercase">you</h2>
                </div>
                <div className="form-group">
                  <label htmlFor="type">Choose attack type</label>
                  <select name="type" className="form-control" id="type">
                    {typeOptions.map(({ name, value }) => (
                      <option key={value} value={value}>{name}</option>
                    ))}
                  </select>
                </div>
                <div className="form-group">
                  <label htmlFor="attack">Attack power</label>
                  <input name="attack" type="number" min="0" className="form-control" id="attack" />
                </div>
                <img src="img/charmander.png" alt="Charmander" className="img-fluid" />
              </div>
              <div className="col col-lg-5 offset-lg-1">
                <img src="img/pikachu.png" alt="Pikachu" className="img-fluid" />
                <div className="form-group">
                  <label htmlFor="opponentType">Choose attack type</label>
                  <select name="opponentType" className="form-control" id="opponentType">
                    {typeOptions.map(({ name, value }) => (
                      <option key={value} value={value}>{name}</option>
                    ))}
                  </select>
                </div>
                <div className="form-group">
                  <label htmlFor="defense">Defense level</label>
                  <input name="defense" type="number" min="0" className="form-control" id="defense" />
                </div>
                <div>
                  <h2 className="text-uppercase">Opponent</h2>
                </div>
              </div>
            </div>
            <div className="row">
              {submit && !valid ?
                <div className="col my-4">
                  <span className="text-alert">You must complete all fields to fight</span>
                </div>
                : null}
            </div>
            <div className="row">
              <div className="col mt-4">
                <button type="submit" className="btn btn-primary">Attack!</button>
              </div>
            </div>
          </form>
        </div>
      </div>
      <div className="row justify-content-center">
        <div className="col col-lg-11 mt-4">
          <div className="message-board">
            {damage !== 0 ?
              <h2>A critical hit of {damage} points!</h2>
              : <h2>Your hit got {damage} points, not enough to win!</h2>
            }
          </div>
        </div>
      </div>
    </div>
  )
}

export default Home