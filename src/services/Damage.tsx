interface args {
  type: string,
  attack: string,
  defense: string,
  opponentType: string
}

const getDamage = async (props: args) => {

  const { type, attack, defense, opponentType } = props

  const url = 'http://localhost:4000/moves/damage?attack=' + attack + '&defense=' + defense + '&type=' + type + '&opponentType=' + opponentType

  const res = await fetch(url)

  if (res.ok) {
    const damage = await res.json()
    return parseInt(damage.total)
  }
  else return 0
}

export { getDamage }