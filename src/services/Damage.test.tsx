import { getDamage } from './Damage'

describe('Testing over Damage services', () => {
  
  test('Should get damage value 50', async () => {
    const args = {
      type: 'fire',
      opponentType: 'water',
      attack: '20',
      defense: '10'
    }
    const damage = await getDamage(args)
    expect(damage).toBe(50)
  })

  test('Should get damage value 0', async () => {
    const args = {
      type: 'fire',
      opponentType: 'fire',
      attack: '0',
      defense: '0'
    }
    const damage = await getDamage(args)
    expect(damage).toBe(0)
  })
})